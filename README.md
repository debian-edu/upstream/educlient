# Maintaining the cross-distro Edu Client package #

## Introduction ##

This package provides configuration files and installation scripts for
turning a Debian / Ubuntu / Linux Mint /LMDE workstation into a Debian
Edu network client.

This package provides legacy support for previous Debian/Ubuntu/Linux
Mint/LMDE versions and shall always do so.

However, this makes the maintenance of the Edu Client package a little
bit tricky. This package needs to assume upcoming changes in Debian
derivative distributions and adapt to them before that derivative distro
becomes released. A post-release upload of this package to a Debian
derivative distribution is very unlikely and the package's design tries
to avoid this becoming necessary.

## Package not working on your distribution (version) ##

... for users of this package.

Support distributions are:

  * Debian (since Debian 7 aka wheezy)
  * Ubuntu (since Ubuntu 12.04 aka precise)
  * Linux Mint (since Linux Mint 17.1 aka Rebecca)
  * LMDE (since LMDE 2 aka Betsy)

The Edu Client package will _only_ support LTS distributions. Don't even
try this package on non-LTS distributions. You are likely to fail. If you
use a non-LTS Linux distro, please start scratching your head and ask
yourself why you do such silly things.

If this package is broken in your Debian derivative LTS distribution,
please try obtaining this package from Debian unstable directly and (if a
newer version is available in Debian unstable) check if educlient from
Debian unstable fixes observed issues on your system.

If using the package version from Debian unstable does not work either,
please get in touch with us developers.

## The maintenance concept ##

... for package maintainers.

We assume that all derivative Debian distros are derived from Debian
unstable. Thus, if our package works on Debian testing/unstable, it is
very likely that it will work on the Debian derivative distribution.

However, we also want to provide legacy support for older distribution
versions, so package maintainers should follow the below workflow:

  1. Adapt this package to changes in Debian unstable via maintaining
     configuration files in the etc-debian-unstable/ folder of this
     package.

  2. If the made change(s) modify any of the configuration files shipped
     in the educlient package, provide an old copy of that configuration
     file in

     ´´´
     etc-distrib-legacy/<distrib>/<version>
     ´´´

     Provide that legacy configuration file version for all distributions
     and all distribution versions that are older than the next upcoming
     releases of those distributions. (I.e., provide a copy of that old
     file version in all directories under etc-distrib-legacy/.

  3. Please use symlinks for referencing the same file version in the     
     etc-distrib-legacy/ folder.

     If files in Linux Mint and Ubuntu match, please place the file in
     one of the Ubuntu subfolders and symlink from Linux Mint to Ubuntu.

     If files in LMDE and Debian match, please place the file in
     one of the Debian subfolders and symlink from LMDE to Debian.

     Symlinks in etc-distrib-legacy/ folder: files may be symlinked.
     DON'T symlink folders.

  4. If there are configuration files that are only specific to one or
     several distribution (e.g. upstart configuration files in Ubuntu
     and Linux Mint), please place those configuration files into the
     etc-distrib-legacy/<distrib>/latest/ subfolder.

## Questions / Feedback ##

If you have any questions, please contact Mike Gabriel / Giorgio Pioda
via the Debian Edu mailinglist: https://lists.debian.org/debian-edu.

If you want to give feedback on this package (i.e., report observed
bugs), please use the Debian bugtracker for package "educlient". Thanks.

